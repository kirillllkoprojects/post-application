export const CREATE_USER = 'create_user';
export const DELETE_USER = 'delete_user';

export function createUser(user) {
    return {
        type: CREATE_USER,
        payload: user
    };
}

export function deleteUser(user) {
    return {
        type: DELETE_USER,
        payload: user
    };
}