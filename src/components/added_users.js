import _ from 'lodash';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {deleteUser} from '../actions';

class AddedUsers extends Component {
    onDeleteClick(id) {
        this.props.deleteUser(id)
    }

    renderUsers() {
        return _.map(this.props.users, user => {
            return (
                    <li className="list-group-item" key={user.name}>
                        <button onClick={() => this.onDeleteClick(user.name)}
                                className="btn btn-danger btn-sm pull-xs-right delete-btn"
                        >
                            Delete
                        </button>
                        <div>{user.name} {user.lastName}</div>
                    </li>
            );
        });
    }

    render() {
        return (
            <div className="col-sm-6">
                <h3>Users</h3>
                <ul className="list-group">
                    {this.renderUsers()}
                </ul>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {users: state.users};
}

export default connect(mapStateToProps, {deleteUser})(AddedUsers);