import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {createUser} from '../actions';

class NewUser extends Component {

    static renderField(field) {
        const {meta: {touched, error}} = field;
        const className = `form-group ${touched && error ? 'has-danger' : ''}`;

        return (
            <div className={className}>
                <label>{field.label}</label>
                <input
                    className="form-control"
                    type="text"
                    {...field.input}
                    placeholder={field.placeholder}
                />
                <div className="text-help">
                    {touched ? error : ''}
                </div>
            </div>
        );
    }

    onSubmit(values) {
        this.props.createUser(values)
    }

    render() {
        const {handleSubmit} = this.props;

        return (
            <div className="col-sm-6">
                <h3>Create new user</h3>
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <Field
                        placeholder="Enter name"
                        label="Name"
                        name="name"
                        component={NewUser.renderField}
                    />
                    <Field
                        placeholder="Enter last name"
                        label="Last Name"
                        name="lastName"
                        component={NewUser.renderField}
                    />
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        );
    }
}

function validate(values) {
    const errors = {};
    if (!values.name) {
        errors.name = "Enter a name!";
    }
    if (!values.lastName) {
        errors.lastName = "Enter a last name!";
    }
    return errors;
}

export default reduxForm({
    validate,
    form: 'NewUser'
})(
    connect(null, {createUser})(NewUser)
);