import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from 'redux';
import {BrowserRouter} from 'react-router-dom';
import promise from 'redux-promise';

import reducers from './reducers';
import AddedUsers from './components/added_users';
import NewUser from './components/new_user';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <BrowserRouter>
            <div className="col-md-12">
                <div className="row">
                    <NewUser/>
                    <AddedUsers/>
                </div>
            </div>
        </BrowserRouter>
    </Provider>
    , document.querySelector('.container'));
