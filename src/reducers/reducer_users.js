import _ from 'lodash';
import {CREATE_USER, DELETE_USER} from '../actions';

export default function (state = {}, action) {
    switch (action.type) {
        case CREATE_USER:
            return {...state, [action.payload.name]: action.payload};
        case DELETE_USER:
            return _.omit(state, action.payload);
        default:
            return state;
    }

}